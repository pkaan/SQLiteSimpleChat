package com.peetukaan.preassignment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Messenger {
	
	public static void main(String[] args) throws SQLException {
		
		Connection database = null;
	
		try {
		
			database = DriverManager.getConnection("jdbc:sqlite:messages.db");
			
			System.out.println("Connection established!");
			
			PreparedStatement table  = database.prepareStatement("CREATE TABLE IF NOT EXISTS Messages (\n"  
		            + " id INTEGER PRIMARY KEY,\n"  
		            + " name TEXT NOT NULL,\n"  
		            + " message TEXT NOT NULL,\n"
		            + " timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP \n"
		            + ");");
			
		  	table.executeUpdate();
		  	table.close();
			
			Scanner scanner = new Scanner(System.in);
			
			System.out.print("Enter username> ");
			
			String name = scanner.nextLine();
			
			System.out.println("Hello " + name + "!");
			
			loop:
				while (true) {
					System.out.println("1. Write a message");
					System.out.println("2. Read last message sent by the user " + name);
					System.out.println("3. Read all the messages sent by the user " + name);
					System.out.println("4. Read all the messages sent by all the users");
					System.out.println("5. Change username");
					System.out.println("6. Exit");
					System.out.print(">");

					int choice = Integer.valueOf(scanner.nextLine());
					switch(choice) {
					case 1:
						System.out.print(name + ">");
						String message = scanner.nextLine();
				        PreparedStatement statement = database.prepareStatement("INSERT INTO Messages(name,message) VALUES (?,?)");
				        statement.setString(1, name);
				        statement.setString(2, message);
				        try {
				        	statement.executeUpdate();
					        System.out.println("Message sent!");
				        } catch (SQLException e) {
							System.out.println("Error: " + e.getMessage());
				        }
				        if (statement != null) {
					        statement.close();
				        }
				        break;
					case 2:
						System.out.println("Retrieving message..."); 			
						PreparedStatement statement2 = database.prepareStatement("SELECT * from Messages WHERE name =? order by ROWID DESC limit 1");
					    statement2.setString(1,name);
						try {
						    ResultSet result2 = statement2.executeQuery();
					        System.out.println("(" + result2.getString("timestamp") + ") " + "<" + result2.getString("name") + "> " + result2.getString("message"));
						} catch (SQLException e) {
				            System.out.println("No message found");
						}
				        if (statement2 != null) {
					        statement2.close();
				        }
					    break;
					case 3:
						System.out.println("Retrieving messages..."); 
						PreparedStatement statement3 = database.prepareStatement("SELECT * from Messages WHERE name =?");
					    statement3.setString(1,name);
						try {
						    ResultSet result3 = statement3.executeQuery();
						    while (result3.next()) {
						        System.out.println("(" + result3.getString("timestamp") + ") " + "<" + result3.getString("name") + "> " + result3.getString("message"));
						    }
						} catch (SQLException e) {
				            System.out.println("No messages found");
						}
				        if (statement3 != null) {
					        statement3.close();
				        }
					    break;
					case 4:
						System.out.println("Retrieving messages...");		
						PreparedStatement statement4 = database.prepareStatement("SELECT * from Messages");
						try {
						    ResultSet result4 = statement4.executeQuery();
						    while (result4.next()) {
						        System.out.println("(" + result4.getString("timestamp") + ") " + "<" + result4.getString("name") + "> " + result4.getString("message"));
						    }
						} catch (SQLException e) {
				            System.out.println("No messages found");
						}
				        if (statement4 != null) {
					        statement4.close();
				        }
					    break;
					case 5:
						System.out.print("Enter username> ");
						name = scanner.nextLine();
						break;
					case 6: 
						break loop;
					default:
						break;
					}
				}
		
		if (scanner != null) {
			scanner.close();
		}   
		
		System.out.println("Closing connection...");
		
		if (database != null) {
			database.close();
		}
		
		System.out.println("Connection closed.");
		    
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
}
