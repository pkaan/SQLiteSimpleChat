Compile using command line (Windows):

(using exec-maven-plugin)

> mvn exec:java

OR

(maven-assembly-plugin)

> mvn assembly:assembly -DdescriptorId=jar-with-dependencies

> java -cp target/preassignment-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.peetukaan.preassignment.Messenger
